# Overview #

This repository contains the source code for a library created using the .Net Framework. It provide a solution to manage your personal library

### Features ###

 * Adding books to the library
 * Removing books from the library
 * Looking up books using arbitrary conditions
 * Use a generic class to allow storing/retrieving other object types in a strongly typed manner.
 * Use the repository pattern to decouple the way the data is stored on disk from the way the data is accessed via the api. This allows other storage mechanisms to be implemented with minimal changes to the unit tests.

### Environment Setup ###

 * Install Xamarin Studio or Visual Studio
 * Open the .sln file in the root of the project
 * Download the source code of the application from [downloads page](https://bitbucket.org/jakestateresa/healthengine.bookrepository/downloads) or clone the repository using the command ```git clone https://bitbucket.org/jakestateresa/healthengine.bookrepository.git```
 * Extract the source code in a suitable directory

### Running Unit Tests ###

 * Right click on the HealthEngine.Books.Tests project and select Run Item.