﻿using NUnit.Framework;
using System;
using System.Linq;

namespace HealthEngine.Books.Tests
{
	[TestFixture]
	public class RepositoryTests
	{
		IRepository<Book> repository = null;

		[SetUp]
		public void Setup()
		{
			repository = new InMemoryRepository<Book>();
			repository.Add(new Book
			{
				Id = Guid.NewGuid().ToString(),
				Title = "The Pragmatic Programmer",
				Author = "Andy Hunt and Dave Thomas"
			});

			repository.Add(new Book
			{
				Id = Guid.NewGuid().ToString(),
				Title = "Code Complete",
				Author = "Steve McConnell"
			});

			repository.Add(new Book
			{
				Id = Guid.NewGuid().ToString(),
				Title = "CLR Via C#",
				Author = "Jeffrey Richter"
			});

			repository.Add(new Book
			{
				Id = Guid.NewGuid().ToString(),
				Title = "Clean Code",
				Author = "Robert C. Martin"
			});

			repository.Add(new Book
			{
				Id = Guid.NewGuid().ToString(),
				Title = "Refactoring: Improving the Design of Existing Code",
				Author = "Martin Fowler , Kent Beck , John Brant, William Opdyke, Don Roberts, Erich Gamma"
			});

			repository.Add(new Book
			{
				Id = Guid.NewGuid().ToString(),
				Title = "The Clean Coder",
				Author = "Robert C. Martin"
			});

			repository.Add(new Book
			{
				Id = Guid.NewGuid().ToString(),
				Title = "Patterns of Enterprise Application Architecture",
				Author = "Martin Fowler"
			});
		}

		[Test]
		public void AddTest()
		{
			var foundItems = repository.Lookup(book => book.Title =="Head First Design Patterns");
			Assert.AreEqual(0, foundItems.Count());

			repository.Add(new Book
			{
				Id = Guid.NewGuid().ToString(),
				Title = "Head First Design Patterns",
				Author = "Eric Freeman, Bert Bates, Kathy Sierra (Author), Elisabeth Robson"
			});

			foundItems = repository.Lookup(book => book.Title == "Head First Design Patterns");
			Assert.AreEqual(1, foundItems.Count());
		}

		[Test]
		public void EnsureAddAllowsDuplicates()
		{
			var foundItems = repository.Lookup(book => book.Title == "Head First Design Patterns");
			Assert.AreEqual(0, foundItems.Count());

			repository.Add(new Book
			{
				Id = Guid.NewGuid().ToString(),
				Title = "Head First Design Patterns",
				Author = "Eric Freeman, Bert Bates, Kathy Sierra (Author), Elisabeth Robson"
			});

			foundItems = repository.Lookup(book => book.Title == "Head First Design Patterns");
			Assert.AreEqual(1, foundItems.Count());

			repository.Add(new Book
			{
				Id = Guid.NewGuid().ToString(),
				Title = "Head First Design Patterns",
				Author = "Eric Freeman, Bert Bates, Kathy Sierra (Author), Elisabeth Robson"
			});

			foundItems = repository.Lookup(book => book.Title == "Head First Design Patterns");
			Assert.AreEqual(2, foundItems.Count());
		}

		[Test]
		public void RemoveTests()
		{
			var foundItems = repository.Lookup(book => book.Author == "Robert C. Martin");
			Assert.AreEqual(2, foundItems.Count());

			foreach (var item in foundItems.ToList())
			{
				repository.Remove(item);
			}

			foundItems = repository.Lookup(book => book.Author == "Robert C. Martin");
			Assert.AreEqual(0, foundItems.Count());
		}

		[Test]
		public void EnsureRemoveOnlyRemovesTheFirstItemInTheList()
		{
			var foundItems = repository.Lookup(book => book.Title == "Head First Design Patterns");
			Assert.AreEqual(0, foundItems.Count());

			var firstGuid = Guid.NewGuid().ToString();
			repository.Add(new Book
			{
				Id = firstGuid,
				Title = "Head First Design Patterns",
				Author = "Eric Freeman, Bert Bates, Kathy Sierra (Author), Elisabeth Robson"
			});

			foundItems = repository.Lookup(book => book.Title == "Head First Design Patterns");
			Assert.AreEqual(1, foundItems.Count());

			var secondGuid = Guid.NewGuid().ToString();
			repository.Add(new Book
			{
				Id = secondGuid,
				Title = "Head First Design Patterns",
				Author = "Eric Freeman, Bert Bates, Kathy Sierra (Author), Elisabeth Robson"
			});

			foundItems = repository.Lookup(book => book.Title == "Head First Design Patterns");
			Assert.AreEqual(2, foundItems.Count());

			repository.Remove(foundItems.First());
			foundItems = repository.Lookup(book => book.Title == "Head First Design Patterns");
			Assert.AreEqual(1, foundItems.Count());
			Assert.AreEqual(secondGuid, foundItems.First().Id);
		}
	}
}
