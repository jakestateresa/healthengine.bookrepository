﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace HealthEngine.Books
{
	/// <summary>
	/// Represents a strongly typed list of objects that can be accessed using a filter expression. Provides methods to lookup, add, and remove items.
	/// </summary	
	public interface IRepository<TItem>
	{
		/// <summary>
		/// Lookup the specified items in the Repository using a filter expression, page and pageSize.
		/// </summary>
		/// <returns>The list of items found.</returns>
		/// <param name="filter">Filter expression</param>
		IEnumerable<TItem> Lookup(Expression<Func<TItem, bool>> filter = null);

		/// <summary>
		/// Adds an object to the the Reposiory<T>.
		/// </summary>
		void Add(TItem item);

		/// <summary>
		/// Removes an object to the the Reposiory<T>.
		/// </summary>
		void Remove(TItem item);
	}
}