﻿namespace HealthEngine.Books
{
	/// <summary>
	/// Provides information about a book.
	/// </summary>
	public class Book
	{
		/// <summary>
		/// Gets or sets the id of the book
		/// </summary>
		public string Id { get; set; }

		/// <summary>
		/// Gets or sets the title of the book
		/// </summary>
		public string Title { get; set; }

		/// <summary>
		/// Gets or sets the title of the book
		/// </summary>
		public string Author { get; set; }
	}
}
