﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace HealthEngine.Books
{
	/// <summary>
	/// A class representing an strongly typed repository of items stored in-memory.
	/// This class stored the items using a list as a backing store.
	/// </summary>
	public class InMemoryRepository<TItem> : IRepository<TItem>
	{
		List<TItem> items = new List<TItem>();

		/// <summary>
		/// Add the specified item to the end of the list. Complexity O(1)
		/// </summary>
		/// <param name="item">Item to add</param>
		public virtual void Add(TItem item)
		{
			items.Add(item);
		}

		/// <summary>
		/// Lookup the specified items in the Repository using a filter expression. Complexity O(n)
		/// This operation allows the search to be performed on arbitrary properties of the item while
		/// still ensuring type safety.
		/// </summary>
		/// <returns>The list of items found.</returns>
		/// <param name="filter">Filter expression</param>
		public virtual IEnumerable<TItem> Lookup(Expression<Func<TItem, bool>> filter = null)
		{
			return items.Where(filter.Compile());
		}

		/// <summary>
		/// Removes the first occurrence of a specific item. Complexity O(n)
		/// </summary>
		/// <param name="item">Item to remove</param>
		public virtual void Remove(TItem item)
		{
			items.Remove(item);
		}
	}
}
